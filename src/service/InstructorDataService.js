import axios from 'axios'

//const INSTRUCTOR = 'tsoft'
const INSTRUCTOR_API_URL = 'http://localhost:8080/api/v1/instructors'


class InstructorDataService {

    retrieveAllInstructors() {
        return axios.get(INSTRUCTOR_API_URL);
    }

    createInstructor(instructor) {
        return axios.post(INSTRUCTOR_API_URL, instructor);
    }

    retrieveInstructor(id) {
        return axios.get(INSTRUCTOR_API_URL + '/' + id);
    }

    updateInstructor(id, instructor) {
        return axios.put(INSTRUCTOR_API_URL + '/' + id, instructor);
    }


    deleteInstructor(id) {
        //console.log('executed service')
        return axios.delete(INSTRUCTOR_API_URL + '/' + id);
    }
}


export default new InstructorDataService()
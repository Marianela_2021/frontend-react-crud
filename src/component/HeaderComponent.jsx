import React, { Component } from 'react'

class HeaderComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        return (
           
            <header>
                <nav className="navbar navbar-expand navbar-dark bg-dark">
                    <div className="container-fluid">
                        <a className="navbar-brand fw-bold text-white fs-1" href="/">Instructors Aplication</a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="nav nav-tabs me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <a className="nav-link text-white" href="/instructors">Instructors</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-white" href="/courses">Cursos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            
        )
    }
}

export default HeaderComponent
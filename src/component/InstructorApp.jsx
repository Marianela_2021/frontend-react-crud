import React, { Component } from 'react';
import ListCoursesComponent from './courses/ListCoursesComponent';
import CourseComponent from './courses/CourseComponent';
import ListInstructorsComponent from './instructors/ListInstructorsComponent';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import InstructorComponent from './instructors/InstructorComponent';
import ListCoursesByUserComponent from './ListCoursesByUserComponent';


class InstructorApp extends Component { 
    render() {
        return (

            <BrowserRouter>
                <>
                <Switch>
                    <Route path="/" exact component={ListCoursesComponent} />
                    <Route path="/courses" exact component={ListCoursesComponent} />
                    <Route path="/courses/:id" exact component={CourseComponent} />
                    <Route path="/instructors" exact component={ListInstructorsComponent} />
                    <Route path="/instructors/:id" exact component={InstructorComponent} />
                    <Route path="/instructors/:username/coursesByUser" exact component={ListCoursesByUserComponent} />
                </Switch>
                </>
                    
            </BrowserRouter>
        )
    }
}
export default InstructorApp
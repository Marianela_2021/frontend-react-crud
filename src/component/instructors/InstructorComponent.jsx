import { Field, Form, Formik, ErrorMessage } from "formik";
import React, { Component } from "react";
import InstructorDataService from "../../service/InstructorDataService";



class InstructorComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            username: '',
            nombre: '',
            apellido: ''
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)
    }

    componentDidMount() {

        console.log(this.state.id)
        // eslint-disable-next-line
        if (this.state.id == -1) {
            return
        }

        InstructorDataService.retrieveInstructor(this.state.id)
            .then(response => this.setState({
                username: response.data.username,
                nombre: response.data.nombre,
                apellido: response.data.apellido
            }))
    }

    onSubmit(values) {
        //let username = INSTRUCTOR

        let instructor = {
            id: this.state.id,
            username: values.username,
            nombre: values.nombre,
            apellido: values.apellido,
            targetDate: values.targetDate
        }

        
        if (this.state.id === '-1') {
            InstructorDataService.createInstructor(instructor)
                .then(() => this.props.history.push('/instructors'))
        } else {
            InstructorDataService.updateInstructor(this.state.id, instructor)
                .then(() => this.props.history.push('/instructors'))
        }

        console.log(values);
    }

    validate(values) {
        let errors = {}
        if (!values.nombre) {
            errors.nombre = 'Ingrese un Nombre'
        } else if (values.nombre.length < 5) {
            errors.nombre = 'Ingrese al menos 5 caracteres en Nombre'
        }

        return errors
    }

    back() {
        this.props.history.push('/instructors');
    }



    render() {

        let { id, username, nombre, apellido } = this.state

        return (
            <div>
                <h3>Instructor</h3>
                <div className="container">
                    <Formik initialValues={{ id, username, nombre, apellido }}
                            onSubmit={this.onSubmit}
                            validateOnChange={false}
                            validateOnBlur={false}
                            validate={this.validate}
                            enableReinitialize={true}>
                        {(props) => (
                            <Form>
                                <fieldset className="form-group">
                                    <label>Id</label>
                                    <Field className="form-control" type="text" name="id" disabled />
                                </fieldset>

                                <fieldset className="form-group">
                                    <label>Nombre de Usuario</label>
                                    <Field className="form-control" type="text" name="username" />
                                </fieldset>

                                <fieldset className="form-group">
                                    <label>Nombre</label>
                                    <Field className="form-control" type="text" name="nombre" />
                                    <ErrorMessage name="nombre" component="div"
                                        className="alert alert-warning" />
                                </fieldset>

                                <fieldset className="form-group">
                                    <label>Apellido</label>
                                    <Field className="form-control" type="text" name="apellido" />
                                </fieldset>

                                <button className="btn btn-success" type="submit">Save</button>
                                <button className="btn btn-primary" onClick={this.back.bind(this)} style={{ marginLeft: "10px" }}>Back</button>
                            </Form>
                            )}
                    </Formik>
                </div>
            </div>
            )
    }
}
export default InstructorComponent
import React, { Component } from "react";
import InstructorDataService from "../../service/InstructorDataService";


class ListinstructorsComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            instructors: [],
            instructorsBackup: [],
            textBuscar: "",
            message: null
        }
        this.refreshInstructors = this.refreshInstructors.bind(this)
        this.addInstructorClicked = this.addInstructorClicked.bind(this)
        this.updateInstructorClicked = this.updateInstructorClicked.bind(this)
        this.deleteInstructorClicked = this.deleteInstructorClicked.bind(this)
        this.cursosXInstructorClicked = this.cursosXInstructorClicked.bind(this)
    }

    componentDidMount() {
        this.refreshInstructors();
    }


    refreshInstructors() {
        InstructorDataService.retrieveAllInstructors().then(
            response => {
                console.log(response);
                this.setState({ instructors: response.data, instructorsBackup: response.data })
            })
    }

    addInstructorClicked() {
        this.props.history.push(`/instructors/-1`)
    }

    updateInstructorClicked(id) {
        console.log('update ' + id)
        this.props.history.push(`/instructors/${id}`)
    }

    deleteInstructorClicked(id) {
        InstructorDataService.deleteInstructor(id)
            .then(
                response => {
                    this.setState({ message: `Delete of instructor ${id} Successful` })
                    this.refreshInstructors()
                }
            )
    }

    cursosXInstructorClicked(username) {
        this.props.history.push(`/instructors/${username}/coursesByUser`)
    }

    filter(event) {

        //console.log(text.target.value);
        var text = event.target.value
        const data = this.state.instructorsBackup
        const newData = data.filter(function (item) {
            const itemDataUsername = item.username.toUpperCase()
            const itemDataNombre = item.nombre.toUpperCase()
            const itemDataApellido = item.apellido.toUpperCase()
            const campo = itemDataUsername + " " + itemDataNombre + " " + itemDataApellido
            const textData = text.toUpperCase()
            return campo.indexOf(textData) > -1
        })
        this.setState({
            instructors: newData,
            textBuscar: text,
        })
    }



    render() {
        return (
            <div className="container">
                <h3>All Instructors</h3>
                {this.state.message && <div className="alert alert-success">{this.state.message}</div>}

                <div className="row">
                    <div className=" d-flex justify-content-start col-6 mb-3 mt-5">
                        <form>
                            <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" value={this.state.text}
                                onChange={(text) => this.filter(text)} />
                         </form>
                    </div>
                    <div className=" d-flex justify-content-end col-6 mb-3 mt-5">
                        <button className="btn btn-success" onClick={() => this.addInstructorClicked()}>Add</button>
                    </div>
                </div>
               
                <table className="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Username</th>
                            <th>Name</th>
                            <th>Lastname</th>
                            <th>Assigned Courses</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.instructors.length>0 &&
                            this.state.instructors.sort(({ id: prevId }, { id: currId }) => prevId - currId).map(
                                instructor =>
                                    <tr key={instructor.id}>
                                        <td>{instructor.id}</td>
                                        <td>{instructor.username}</td>
                                        <td>{instructor.nombre}</td>
                                        <td>{instructor.apellido}</td>
                                        <td><button className="btn btn-light" onClick={() => this.cursosXInstructorClicked(instructor.username)}>Ver</button></td>
                                        <td><button className="btn btn-warning" onClick={() => this.updateInstructorClicked(instructor.id)}>Update</button></td>
                                        <td><button className="btn btn-danger" onClick={() => this.deleteInstructorClicked(instructor.id)}>Delete</button></td>
                                    </tr>
                            )
                        }
                    </tbody>
                </table>
               
            </div>
            )
    }
}

export default ListinstructorsComponent
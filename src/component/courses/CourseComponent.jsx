import React, { Component } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import InstructorDataService from '../../service/InstructorDataService';
import CourseDataService from '../../service/CourseDataService';


const INSTRUCTOR = 'tsoft'

class CourseComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            description: '',
            username: '',
            instructors:[]
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)

    }

    componentDidMount() {

        console.log(this.state.id)

        InstructorDataService.retrieveAllInstructors()
            .then(response => this.setState({ instructors: response.data }),
                console.log(this.state.instructors))

        // eslint-disable-next-line
        if (this.state.id == -1) {
            return
        }

        CourseDataService.retrieveCourse(INSTRUCTOR, this.state.id)
            .then(response => this.setState({
                description: response.data.description,
                username: response.data.username
            }))  
    }

    onSubmit(values) {
        let username = INSTRUCTOR

        let course = {
            id: this.state.id,
            description: values.description,
            username: values.username,
            instructors: values.instructors,
            targetDate: values.targetDate
        }

        if (this.state.id === '-1') {
            CourseDataService.createCourse(username, course)
                .then(() => this.props.history.push('/courses'))
        } else {
            CourseDataService.updateCourse(username, this.state.id, course)
                .then(() => this.props.history.push('/courses'))
        }

        console.log(values);
    }

    validate(values) {
        let errors = {}
        if (!values.description) {
            errors.description = 'Enter a Description'
        } else if (values.description.length < 5) {
            errors.description = 'Enter atleast 5 Characters in Description'
        }

        return errors
    }

    back() {
        this.props.history.push('/courses');
    }

    render() {

        let { description, id, username } = this.state

        return (
            <div>
                <h3>Course</h3>
                <div className="container">
                    <Formik initialValues={{ id, description, username }}
                            onSubmit={this.onSubmit}
                            validateOnChange={false}
                            validateOnBlur={false}
                            validate={this.validate}
                            enableReinitialize={true}>
                        {(props) => (
                            <Form>
                                <fieldset className="form-group">
                                    <label>Id</label>
                                    <Field className="form-control" type="text" name="id" disabled />
                                </fieldset>
                                <fieldset className="form-group">
                                    <label>Description</label>
                                    <Field className="form-control" type="text" name="description" />
                                    <ErrorMessage name="description" component="div"
                                        className="alert alert-warning" />
                                </fieldset>
                                <fieldset className="form-group">
                                    <label>Instructor</label>
                                    <Field as="select" className="custom-select" name="username" >
                                        <option value="0">Seleccionar</option>
                                            {
                                            this.state.instructors.map(instructor => (
                                                <option key={instructor.id} value={instructor.username}>{instructor.username}</option>
                                            ))
                                        }
                                    </Field>
                                </fieldset>
                                <button className="btn btn-success" type="submit">Save</button>
                                <button className="btn btn-primary" onClick={this.back.bind(this)} style={{ marginLeft: "10px" }}>Back</button>
                            </Form>
                        )}
                        
                    </Formik>

                </div>
            </div>
        )
    }
}

export default CourseComponent
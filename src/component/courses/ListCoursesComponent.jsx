import React, { Component } from 'react';
import CourseDataService from '../../service/CourseDataService';


const INSTRUCTOR = 'tsoft';

class ListCoursesComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            courses: [],
            coursesBackup: [],
            textBuscar: "",
            message: null
        }
        this.refreshCourses = this.refreshCourses.bind(this)
        this.updateCourseClicked = this.updateCourseClicked.bind(this)
        this.deleteCourseClicked = this.deleteCourseClicked.bind(this)
        this.addCourseClicked = this.addCourseClicked.bind(this)
    }

    componentDidMount() {
        this.refreshCourses();
    }

    refreshCourses() {

        CourseDataService.retrieveAllCourses(INSTRUCTOR)//HARDCODED
            .then(
                response => {
                    console.log(response);
                    this.setState({ courses: response.data, coursesBackup: response.data })
                }
            )
    }


    updateCourseClicked(id) {
        console.log('update ' + id)
        this.props.history.push(`/courses/${id}`)
    }

    deleteCourseClicked(id) {
        CourseDataService.deleteCourse(INSTRUCTOR, id)
            .then(
                response => {
                    this.setState({ message: `Delete of course ${id} Successful` })
                    this.refreshCourses()
                }
            )
    }

    addCourseClicked() {
        this.props.history.push(`/courses/-1`)
    }


    filter(event) {

        //console.log(text.target.value);
        var text = event.target.value
        const data = this.state.coursesBackup
        const newData = data.filter(function (item) {
            const itemDataUsername = item.username.toUpperCase()
            const itemDataDescription = item.description.toUpperCase()
            const campo = itemDataUsername + " " + itemDataDescription
            const textData = text.toUpperCase()
            return campo.indexOf(textData) > -1
        })
        this.setState({
            courses: newData,
            textBuscar: text,
        })
    }

    render() {

        //const { searchCourse } = this.state;
        return (
            <div className="container">
                <h3>All Courses</h3>
                {this.state.message && <div className="alert alert-success">{this.state.message}</div>}

                <div className="row">
                    <div className=" d-flex justify-content-start col-6 mb-3 mt-5">
                        <form>
                            <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" value={this.state.text}
                                onChange={(text) => this.filter(text)} />
                        </form>
                    </div>
                    <div className=" d-flex justify-content-end col-6 mb-3 mt-5">
                        <button className="btn btn-success" onClick={this.addCourseClicked}>Add</button>
                    </div>
                </div>
                
                <div className="container">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Description</th>
                                <th>Instructor</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.courses.length > 0 &&
                                this.state.courses.sort(({ id: prevId }, { id: currId }) => prevId - currId).map(
                                    course =>
                                        <tr key={course.id}>
                                            <td>{course.id}</td>
                                            <td>{course.description}</td>
                                            <td>{course.username}</td>
                                            <td><button className="btn btn-warning" onClick={() => this.updateCourseClicked(course.id)}>Update</button></td>
                                            <td><button className="btn btn-danger" onClick={() => this.deleteCourseClicked(course.id)}>Delete</button></td>
                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
 
}
export default ListCoursesComponent
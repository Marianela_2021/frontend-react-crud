import React, { Component } from 'react';
import CourseDataService from '../service/CourseDataService';

const INSTRUCTOR = 'tsoft';

class ListCoursesByUserComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: this.props.match.params.username,
            courses: [],
            message: null
        }
        this.refreshCourses = this.refreshCourses.bind(this)
        this.updateCourseClicked = this.updateCourseClicked.bind(this)
        this.deleteCourseClicked = this.deleteCourseClicked.bind(this)
        this.addCourseClicked = this.addCourseClicked.bind(this)
    }

    componentDidMount() {
        this.refreshCourses(this.state.username);
    }

    refreshCourses(username) {
        CourseDataService.retrieveCoursesByUser(username)
            .then(
                response => {
                    this.setState({ courses: response.data })
                    console.log(this.state.courses);
                }
            )
    }

    updateCourseClicked(id) {
        console.log('update ' + id)
        this.props.history.push(`/courses/${id}`)
    }

    deleteCourseClicked(id) {
        CourseDataService.deleteCourse(INSTRUCTOR, id)
            .then(
                response => {
                    this.setState({ message: `Delete of course ${id} Successful` })
                    this.refreshCourses()
                }
            )
    }

    addCourseClicked() {
        this.props.history.push(`/courses/-1`)
    }

    back() {
        this.props.history.push('/instructors');
    }

    render() {
        return (
            <div className="container">
                <h3>All Courses</h3>
                {this.state.message && <div className="alert alert-success">{this.state.message}</div>}
                <div className="container">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Description</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.courses.length > 0 &&
                                this.state.courses.sort(({ id: prevId }, { id: currId }) => prevId - currId).map(
                                    course =>
                                        <tr key={course.id}>
                                            <td>{course.id}</td>
                                            <td>{course.description}</td>
                                            <td><button className="btn btn-warning" onClick={() => this.updateCourseClicked(course.id)}>Update</button></td>
                                            <td><button className="btn btn-danger" onClick={() => this.deleteCourseClicked(course.id)}>Delete</button></td>
                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                    <div>
                        <button className="btn btn-success" onClick={() => this.addCourseClicked()}>Add</button>
                        <button className="btn btn-primary" onClick={this.back.bind(this)} style={{ marginLeft: "10px" }}>Back</button>
                    </div>
                </div>
            </div>
        )
    }

}
export default ListCoursesByUserComponent
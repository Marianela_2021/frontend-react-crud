import React, { Component } from 'react';
import './App.css';
import HeaderComponent from './component/HeaderComponent';
import InstructorApp from './component/InstructorApp';
class App extends Component {
    render() {
        return (

            <div>
                <div>
                    <HeaderComponent />
                </div>

                <div className="container mt-5">
                    <InstructorApp />
                </div>
            </div>
            
        );
    }
}
export default App;
